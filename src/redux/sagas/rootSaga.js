import { fork, all } from "redux-saga/effects";

import { dashboardSaga } from "./dashboard-saga/dashboard-saga"

export function* rootSaga() {
  yield all([
      fork(dashboardSaga)
  ]);
}
