import "./App.css";
import { Provider } from "react-redux";
import Dashboard from "./containers/dashboard/dashboard"
import Header from "./containers/Header/Header";
import Store from "./redux/store/store";
function App() {

  return (
    <Provider store={Store}>
      <div className="App">
        <Header/>
        <Dashboard />
      </div>
    </Provider>
  );
}

export default App;
