import React from "react";

export default function DetailTile(props) {
  const { data } = props;
  return (
    <div className="tile detail-tile">
      <div className="detail-sub-content">
        <div className="d-flex align-center">
          {/* <img src={stackIcon} alt="stackIcon" className="common-logos" /> */}
          <span className="icons">
            <i class="fas fa-database"></i>
          </span>
          <span className="font-14">Quantity</span>
        </div>
        <div className="font-15 weight-700">{data.Quality}</div>
      </div>
      <div className="detail-sub-content">
        <div className="d-flex align-center">
          <span className="icons">
            <i class="fas fa-at"></i>
          </span>
          <span className="font-14">Avg. Cost</span>
        </div>
        <div className="font-15 weight-700">{data.AvgCost}</div>
      </div>
      <div className="detail-sub-content">
        <div className="d-flex align-center">
          <span className="icons">
            <i class="fas fa-money-bill-alt"></i>
          </span>
          <span className="font-14">Invested Amt</span>
        </div>
        <div className="font-15 weight-700">${data.InvestedAmount}</div>
      </div>
    </div>
  );
}
