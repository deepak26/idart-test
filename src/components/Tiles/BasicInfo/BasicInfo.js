import React from "react";

export default function BasicInfo(props) {
  const { data } = props;
  return (
    <div className="tile basic-tile">
      <div className="hamburger">
        <span className="icons">
          <i class="fas fa-bars"></i>
        </span>
      </div>

      <div className="mf-name">
        <span className="font-10">{data.scrip}</span>
        <span className="color-blue weight-700">${data.Price}</span>
      </div>

      <div className="iShares">
        <span className="font-16 weight-700 color-green">iShares</span>
        <span className="color-light-grey font-8">by-BlockDoc</span>
        <span className="weight-700 font-16">S&P 500 Index</span>
        <span className=" font-10">US Equity</span>
      </div>
    </div>
  );
}
