import React, { useState } from "react";

import "../tiles.css";
export default function UnrealizedPLTile(props) {
  const { data } = props;
  const [isPositive, setPositive] = useState(false);

  const returnCalc = (unrealized, InvestedAmount) => {
    let returnPer = (unrealized * 100) / InvestedAmount;
    return returnPer.toFixed(2);
  };

  return (
    <div className="tile urealized-value-tile">
      <div className="d-flex space-between">
        <span className="font-16 weight-700">Unrealized P/L</span>
        <span className="font-16 weight-700">{data.UnrealizedPL}</span>
      </div>
      <div className="d-flex space-between">
        <span className="color-lightgray">% Return</span>
        <span className="font-16 weight-700">
          {returnCalc(data.UnrealizedPL, data.InvestedAmount) < 0 ? (
            <span className="icons" style={{ color: "red" }}>
              <i class="fas fa-caret-down"></i>
            </span>
          ) : (
            <span className="icons" style={{ color: "lightgreen" }}>
              <i class="fas fa-caret-up"></i>
            </span>
          )}
          {returnCalc(data.UnrealizedPL, data.InvestedAmount)}%
        </span>
      </div>

      <div className="d-flex progress-container">
        <progress
          className="reverse-progress"
          value={
            returnCalc(data.UnrealizedPL, data.InvestedAmount) < 0
              ? Math.abs(returnCalc(data.UnrealizedPL, data.InvestedAmount))
              : "0"
          }
          color="green"
          max="100"
        ></progress>
        <progress
          value={
            returnCalc(data.UnrealizedPL, data.InvestedAmount) > 1
              ? returnCalc(data.UnrealizedPL, data.InvestedAmount)
              : "0"
          }
          color="green"
          max="100"
        ></progress>
      </div>
    </div>
  );
}
