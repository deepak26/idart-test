import React from "react";

export default function MarketValueTile(props) {
  const { data } = props;
  return (
    <div className="tile market-value-tile">
      <div className="d-flex space-between">
        <span className="font-16 weight-700">Market Value</span>
        <span className="font-16 weight-700">${data.Price * data.Quality}</span>
      </div>
      <div className="d-flex space-between">
        <span className="color-lightgray">% of portfolio value</span>
        <span className="font-16 weight-700">
          {data.PercentPortfolioValue}%
        </span>
      </div>
      <div>
        <progress
          value={data.PercentPortfolioValue}
          color="green"
          max="100"
        ></progress>
      </div>
    </div>
  );
}
