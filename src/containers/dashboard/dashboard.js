import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import BasicInfo from "../../components/Tiles/BasicInfo/BasicInfo";
import DetailTile from "../../components/Tiles/DetailTile/DetailTile";
import MarketValueTile from "../../components/Tiles/MarketValueTile/MarketValueTile";
import UnrealizedPLTile from "../../components/Tiles/UnrealizedPLTile/UnrealizedPLTile";
import { dataApiStart } from "../../redux/reducers/dashboard-reducer/dashboard-reducer";


import loader from "../../assets/images/loader.gif"
import {
  PieChart,
  Pie,
  Cell,
  ResponsiveContainer,
  Legend,
} from "recharts";

import "./dashboard.css";

export default function Dashboard() {
  const piedata = [
    { name: "Mutual Funds", value: 2 },
    { name: "ETFs", value: 4 },
  ];
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch({ type: dataApiStart.type, payload: "" });
  }, []);
  const data = useSelector((state) => state.dashboardData.data);
  return (
    <div className="dashboard-wrapper">
      <div className="tile-container">
        {data &&
          data.map((data) => (
            <div className="d-flex tile-row">
              <BasicInfo data={data} />
              <DetailTile data={data} />
              <MarketValueTile data={data} />
              <UnrealizedPLTile data={data} />
              <div className="button-container">
                <button className="cursor">BUY</button>
                <button className="cursor">SELL</button>
              </div>
            </div>
          ))}
        {data.length === 0 && (
          <div className="loader-container">
            <img src={loader} alt="loader" />
          </div>
        )}
      </div>
      <div className="chart-container">
        <div className="chart-wrapper">
          <div className="d-flex space-between">
            <span className="weight-700">Portfolio</span>
            <select>
              <option>Asset wise</option>
            </select>
          </div>
          <ResponsiveContainer width="100%" height={250}>
            <PieChart height={250}>
              <Pie
                data={piedata}
                cx="50%"
                cy="50%"
                outerRadius={80}
                innerRadius={60}
                fill="#8884d8"
                dataKey="value"
              >
                {piedata.map((entry, index) => (
                  <>
                    <Cell fill="#03a9f4" />
                    <Cell fill="#ae9c46" />
                  </>
                ))}
              </Pie>
              <Legend layout="vertical" verticalAlign="middle" align="right" />
            </PieChart>
          </ResponsiveContainer>
        </div>
      </div>
    </div>
  );
}
