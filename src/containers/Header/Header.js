import React from 'react'

import logo from "../../assets/images/idart-logo.jpg"
export default function Header() {
    return (
        <div>
            <header>
                <img src={logo} alt="logo" className="header-logo"/>
            </header>
        </div>
    )
}
